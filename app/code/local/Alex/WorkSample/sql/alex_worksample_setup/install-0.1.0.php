<?php
/**
 * Create a table to store paid orders sum multiplied by a decimal factor
 *
 * @author Oleksii Rybin <oleksii.rybin@gmail.com>
 */
 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()->newTable($installer->getTable('alex_worksample/order_total'));
$table->addColumn(
    'order_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    null,
    array(
        'primary'  => true,
        'unsigned'  => true,
        'nullable'  => false,
    ),
    'Order ID'
);
$table->addColumn(
    'total_paid',
    Varien_Db_Ddl_Table::TYPE_FLOAT,
    null,
    array(),
    'Total Paid Sum'
);
$installer->getConnection()->createTable($table);

$installer->endSetup();