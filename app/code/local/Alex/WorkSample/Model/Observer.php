<?php
/**
 * Observer to log multiplied order total when its fully paid
 *
 * @author Oleksii Rybin <oleksii.rybin@gmail.com>
 */
class Alex_WorkSample_Model_Observer
{
    /**
     * Configuration path for functionality enabled
     */
    const XPATH_ENABLED = 'sales/order_total_log/enabled';
    
    /**
     * Configuration path for order total paid multiplier
     */
    const XPATH_ORDER_TOTAL_MULTIPLIER = 'sales/order_total_log/multiplier';
    
    /**
     * Save multiplied order total paid into a custom DB table
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveOrderTotal(Varien_Event_Observer $observer)
    {
        /** @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoice = $observer->getInvoice();
    
        if (Mage::getStoreConfig(static::XPATH_ENABLED, $invoice->getOrder()->getStoreId())) {
            // log only fully paid orders
            if (!$invoice->getOrder()->getBaseTotalDue()) {
                $totalPaid = $invoice->getOrder()->getBaseTotalPaid();
                $multiplier = Mage::getStoreConfig(static::XPATH_ORDER_TOTAL_MULTIPLIER, $invoice->getOrder()->getStoreId());
                if ($multiplier) {
                    $totalPaid *= (float) str_replace(',', '.', $multiplier);
                }
        
                /** @var $resource Mage_Core_Model_Resource */
                $resource = Mage::getSingleton('core/resource');
                $connection = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_WRITE_RESOURCE);
                $connection->insertOnDuplicate(
                    $resource->getTableName('alex_worksample/order_total'),
                    array(
                        'order_id' => $invoice->getOrder()->getId(),
                        'total_paid' => $totalPaid,
                    ),
                    array(
                        'total_paid' => $totalPaid,
                    )
                );
            }
        }
    }
}